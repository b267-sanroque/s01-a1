<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP - Session 01 Activity</title>
  </head>
  <body>

  	<h1>Full Address</h1>
  	<p><?php echo getFullAddress("Philippines", "Valenzuela", "Metro Manila", "#37 San Felipe St. Karuhatan"); ?></p>

  	<h1>Letter-based Grading</h1>
  	<p><?php echo getLetterGrade(87); ?></p>
  	<p><?php echo getLetterGrade(94); ?></p>
  	<p><?php echo getLetterGrade(74); ?></p>
  </body>
</html>