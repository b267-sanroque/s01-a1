<?php

function getFullAddress($country, $city, $province, $specifics){
	return "$specifics, $city, $province, $country.";
}

function getLetterGrade($grade){
	if($grade < 75){
		return 'D';
	} else if($grade >= 75 && $grade < 77){
		return 'C-';
	} else if($grade >= 77 && $grade < 80){
		return 'C';
	} else if($grade >= 80 && $grade < 83){
		return 'C+';
	} else if($grade >= 83 && $grade < 86){
		return 'B-';
	} else if($grade >= 86 && $grade < 89){
		return 'B';
	} else if($grade >= 89 && $grade < 92){
		return 'B+';
	} else if($grade >= 92 && $grade < 95){
		return 'A-';
	} else if($grade >= 95 && $grade < 98){
		return 'A';
	} else if($grade >= 98 && $grade <= 100 ){
		return 'A+';
	} else{
		return 'not a valid grade number.'
	}
}

